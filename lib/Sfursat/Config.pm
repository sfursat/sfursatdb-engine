package Sfursat::Config;

use strict;
use warnings;

extends 'Config::GitLike';

has 'confname' => (
	default => 'config',
);

has 'compatible' => (
	default => 1,
);

sub is_git_dir {
	my $self = shift;
	my $path = File::Spec->rel2abs( shift );
	$path =~ s{/+$}{};

	($path) = grep {-d} map {"$path$_"} (".git/.git", "/.git", ".git", "");
	return unless $path;

	# Has to have objects/ and refs/ directories
	return unless -d "$path/objects" and -d "$path/refs";

	# Has to have a HEAD file
	return unless -f "$path/HEAD";

	if (-l "$path/HEAD" ) {
		# Symbolic link into refs/
		return unless readlink("$path/HEAD") =~ m{^refs/};
	} else {
		open(HEAD, "$path/HEAD") or return;
		my ($line) = <HEAD>;
		close HEAD;
		# Is either 'ref: refs/whatever' or a sha1
		return unless $line =~ m{^(ref:\s*refs/|[0-9a-fA-F]{20})};
	}
	return $path;
}

=head1 NAME

 Sfursat::Config - sfursat configuration handling

=head1 SYNOPSIS

 Synchronise local SfursatDB database to a CouchDB server

  use Sfursat::Config;
  my $config = Sfursat::Config->new;
  $config->load("/path/to/repo");

=head1 AUTHOR

 Andrea Rota E<lt>hotze@cpan.orgE<gt>

=head1 LICENSE

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

1;

