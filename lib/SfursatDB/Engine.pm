package SfursatDB::Engine;

use strict;
use warnings;
our $VERSION = '0.1';

1;

__END__

=head1 NAME

 SfursatDB::Engine - core SfursatDB engine

=head1 SYNOPSIS

 Synchronise local SfursatDB database to a CouchDB server

  $ sfursat2couch

=head1 AUTHOR

 Andrea Rota E<lt>hotze@cpan.orgE<gt>

=head1 LICENSE

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut
